Delay_1s:
    LDI tmp,255
    LDI tmp2,255
    LDI tmp3,6

Delay_1s_1:
    DEC tmp
    BRNE Delay_1s_1
    
Delay_1s_2:
    DEC tmp2
    BRNE Delay_1s_1

Delay_1s_3:
    DEC tmp3
    BRNE Delay_1s_1
    
    RET 

Delay_200ms:
    LDI tmp3,8
    
Delay_200ms_set_tmp2:
    LDI tmp2,100
Delay_200ms_set_tmp:
    LDI tmp,100
    
Delay_200ms_1:
    DEC tmp
    BRNE Delay_200ms_1
    
Delay_200ms_2:
    DEC tmp2
    BRNE Delay_200ms_set_tmp
    
Delay_200ms_3:
    DEC tmp3
    BRNE Delay_200ms_set_tmp2
    
    RET

Delay_50ms:
    LDI tmp3,2
    
Delay_50ms_set_tmp2:
    LDI tmp2,100
Delay_50ms_set_tmp:
    LDI tmp,100
    
Delay_50ms_1:
    DEC tmp
    BRNE Delay_50ms_1
    
Delay_50ms_2:
    DEC tmp2
    BRNE Delay_50ms_set_tmp
    
Delay_50ms_3:
    DEC tmp3
    BRNE Delay_50ms_set_tmp2
    
    RET
