; I/O functions
;
; Created:  2018.02.04
; Modified: 2018.02.16
; Author:   Martin Stejskal

;=============================| Scan functions |==============================
; Scan A and B channels
; return results in tmp
; Use: tmp, tmp2
Scan_A_B:
    CLR tmp2

    RCALL Scan_A
    
    ; Check if pressed
    CPI tmp,0
    BREQ Scan_A_B_not_pressed_A
    
    ORI tmp2,1
Scan_A_B_not_pressed_A:

    ; Scan channel B
    RCALL Scan_B
    CPI tmp,0
    BREQ Scan_A_B_not_pressed_B
    
    ORI tmp2,2
Scan_A_B_not_pressed_B:

    ; Now put results to tmp
    MOV tmp,tmp2
    RET

; Scan A channel
; Write result to tmp
Scan_A:
    ; Discharge remanent energy in parasite capacity
    ; Set plus to low (disable pull-up)
    CBI PORTB,A_P_PIN
    
    ; Set plus channel as input
    CBI DDRB,A_P_PIN

    ; Set minus to 1 (pull up first)
    SBI PORTB,A_M_PIN

    ; Set minus channel as output
    SBI DDRB,A_M_PIN
    
    ; Give analog some time. Parasite capacity is b!tch
    NOP
    NOP
    
    ; Scan value (fill tmp)
    IN tmp,PINB
    
    ; Get only 0 or non zero value -> filter all other numbers
    ANDI tmp,(1<<A_P_PIN)
    
    RET

; Scan B channel
; Write result to tmp
Scan_B:
    CBI PORTB,B_P_PIN
    CBI DDRB,B_P_PIN
    SBI PORTB,B_M_PIN
    SBI DDRB,B_M_PIN
    NOP
    NOP
    IN tmp,PINB
    ANDI tmp,(1<<B_P_PIN)
    RET


;===============================| LED control |===============================
Enable_LED_A:
    ; Set plus to high and minus to low
    SBI PORTB,A_P_PIN
    CBI PORTB,A_M_PIN
    
    ; Set plus and minus as output
    SBI DDRB,A_P_PIN
    SBI DDRB,A_M_PIN
    
    RET

Disable_LED_A:
    ; Set plus and minus to low
    CBI PORTB,A_P_PIN
    CBI PORTB,A_M_PIN
    
    ; Set plus and minus as output
    SBI DDRB,A_P_PIN
    SBI DDRB,A_M_PIN
    
    RET

Enable_LED_B:
    ; Set plus to high and minus to low
    SBI PORTB,B_P_PIN
    CBI PORTB,B_M_PIN
    
    ; Set plus and minus as output
    SBI DDRB,B_P_PIN
    SBI DDRB,B_M_PIN
    
    RET

Disable_LED_B:
    ; Set plus and minus to low
    CBI PORTB,B_P_PIN
    CBI PORTB,B_M_PIN
    
    ; Set plus and minus as output
    SBI DDRB,B_P_PIN
    SBI DDRB,B_M_PIN
    
    RET

Blink_LED_A_B_5x_200ms:
    RCALL Enable_LED_A
    RCALL Enable_LED_B
    RCALL Delay_200ms
    RCALL Disable_LED_A
    RCALL Disable_LED_B
    RCALL Delay_200ms
    
    RCALL Enable_LED_A
    RCALL Enable_LED_B
    RCALL Delay_200ms
    RCALL Disable_LED_A
    RCALL Disable_LED_B
    RCALL Delay_200ms
    
    RCALL Enable_LED_A
    RCALL Enable_LED_B
    RCALL Delay_200ms
    RCALL Disable_LED_A
    RCALL Disable_LED_B
    RCALL Delay_200ms
    
    RCALL Enable_LED_A
    RCALL Enable_LED_B
    RCALL Delay_200ms
    RCALL Disable_LED_A
    RCALL Disable_LED_B
    RCALL Delay_200ms
    
    RCALL Enable_LED_A
    RCALL Enable_LED_B
    RCALL Delay_200ms
    RCALL Disable_LED_A
    RCALL Disable_LED_B
    RCALL Delay_200ms
    RET

Ringing_LED_A:
    RCALL Enable_LED_A
    RCALL Delay_50ms
    RCALL Disable_LED_A
    RCALL Delay_50ms
    RET
    
Ringing_LED_B:
    RCALL Enable_LED_B
    RCALL Delay_50ms
    RCALL Disable_LED_B
    RCALL Delay_50ms
    RET
