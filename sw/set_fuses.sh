#!/bin/bash
# Fuses:
#  * Int RC Osc: 9.6 MHz divided by 8
#  * Watch dog off
#  * Keep EEPROM settings
#  * SPI enabled
#  * Reset as reset :)
#  * Brown-out detection at 2.7 Vcc
#  * Debug wire off
#  * Self programming disabled

avrdude -c usbasp -p t13 -U lfuse:w:0x2a:m -U hfuse:w:0xfb:m
