; Project settings
;
; Created:  2018.02.04
; Modified: 2018.02.04
; Author:   Martin Stejskal

;===================================| IC |====================================
.NOLIST
.INCLUDE "tn13def.inc"
.LIST

;==================================| Pins |=================================== 
; Side A, plus 
.EQU A_P_PIN = PINB2
; Side A, minus
.EQU A_M_PIN = PINB1

; Side B, plus
.EQU B_P_PIN = PINB3
; Side B, minus
.EQU B_M_PIN = PINB4

;=============================| Register names |==============================
; Usually from R16 to R26

; For temporary storing values
.DEF tmp = R16
.DEF tmp2 = R17
.DEF tmp3 = R18

; Keep state of state machine
.DEF state_mach = R19

; PWM counter
.DEF pwm_cnt = R20
; PWM direction
.DEF pwm_dir = R21

;=============================| PWM definitions |=============================
.EQU pwm_up = 0
.EQU pwm_down = 1


;==============================| State machine |==============================
; Wait for button press (any)
.EQU w84btn_press = 0

; Wait for pressing button on channel A
.EQU w84press_A = 1

; Wait for pressing button on channel B
.EQU w84press_B = 2

