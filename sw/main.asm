; Software for LED bell into our lab
; 
; Created:  2018.02.04
; Modified: 2018.02.16
; Author:   Martin Stejskal

;================================| Settings |=================================
; There are all constants and basic settings or settings related information
.include "lib/setting.asm"

.EQU RESTARTaddr = $000

.ORG RESTARTaddr
    RJMP Init


;===================================| Lib |===================================
.include "lib/delay.asm"
.include "lib/io_itf.asm"

;=============================| Initialization |==============================
Init:
    ; Set initial state for state machine
    LDI state_mach,w84btn_press
    
PWM_Init:
    ; Init timer/PWM
    
    ; Output on pin OC0A
    ; Fast PWM
    LDI tmp,(1<<COM0A1)|(0<<COM0A0)|(1<<WGM01)|(1<<WGM00)
    OUT TCCR0A,tmp
    
    ; Fast PWM (WGM02)
    ; CLK - no Prescaller (1x)
    LDI tmp,(0<<WGM02)|(0<<CS02)|(0<<CS01)|(1<<CS00)
    OUT TCCR0B,tmp
    
    ; Enable output driver (PB0)
    SBI DDRB,0
    
    ; Set PWM counter
    SER pwm_cnt
    ; PWM direction
    LDI pwm_dir,pwm_up
    
    RJMP Main
    
Main_error:
    ; Something went wrong
    RCALL Enable_LED_A
    RCALL Enable_LED_B
    
    RCALL Delay_1s
    
    RCALL Disable_LED_A
    RCALL Disable_LED_B
    
    RCALL Delay_1s

    RJMP Main_error
;==============================| Main function |==============================
Main:
    ; PWM magic
Main_PWM:
    ; Decrease PWM counter. If there is time, change PWM value
    DEC pwm_cnt
    BRNE Main_chck_w84btn_press
Main_PWM_Change_value:
    ; Load value from PWM register
    IN tmp,OCR0A
    
    ; Decrease or increase?
    CPI pwm_dir,pwm_up
    BREQ Main_PWM_Change_value_up
Main_PWM_Change_value_down:

    DEC tmp
    CPI tmp,0
    BRNE Main_PWM_write_val
    ; Else equal zero -> change direction
    LDI pwm_dir,pwm_up    
    RJMP Main_PWM_write_val
    
Main_PWM_Change_value_up:
    INC tmp
    CPI tmp,255
    BRNE Main_PWM_write_val
    ; Else 255 -> change direction
    LDI pwm_dir,pwm_down
    ;RJMP Main_PWM_write_val ; Commented - useless

Main_PWM_write_val:
    OUT OCR0A,tmp
    
    ; Chect actual state
Main_chck_w84btn_press:
    CPI state_mach,w84btn_press
    ; If not -> try to check w84press_A
    BRNE Main_chck_w84press_A
    
    ; if(state_mach == w84btn_press)
    RCALL State_w84btn_press
    RJMP Main

Main_chck_w84press_A:
    CPI state_mach,w84press_A
    BRNE Main_chck_w84press_B
    
    RCALL State_w84press_A
    RJMP Main
    
Main_chck_w84press_B:
    CPI state_mach,w84press_B
    ; If not equal -> unknown state
    BRNE Main_error
    
    RCALL State_w84press_B
    RJMP Main

;=============================| Other functions |=============================
State_w84btn_press:
    ; Scan buttons - result at tmp
    RCALL Scan_A_B
    
    ; If pressed any button, process. Else return
    CPI tmp,0
    BRNE State_w84btn_press_process
    RET
    
State_w84btn_press_process:
    ; Pressed both?
    CPI tmp,0b11
    BRNE State_w84btn_press_proc_prssd_one_btn
    ; Pressed both buttons at same time -> blink to both users and go back to
    ; main function
    RCALL Blink_LED_A_B_5x_200ms
    RET
    
State_w84btn_press_proc_prssd_one_btn:
    ; Pressed one button
    
    ; Pressed button A? If not, jump (BRNE)
    CPI tmp,0b01
    BRNE State_w84btn_press_proc_is_prssd_B

    ; A pressed -> change state -> wait to button on B side will be pressed too
    LDI state_mach,w84press_B
    
    ; Turn on LED A, to let user known that it is "ringing"
    RCALL Enable_LED_A
    RET
    
State_w84btn_press_proc_is_prssd_B:
    CPI tmp,0b10
    BRNE State_w84btn_press_proc_no_btn
    
    ; B button pressed -> change state -> wait for pressing button A
    LDI state_mach,w84press_A
    
    ; Turn on LED B, to let user known that it is "ringing"
    RCALL Enable_LED_B
    RET
    
State_w84btn_press_proc_no_btn:
    ; No button pressed, but this should be checked before -> error
    RJMP Main_error

State_w84press_A:
    ; Scan both buttons
    RCALL Scan_A
    
    CPI tmp,0
    BREQ State_w84press_A_Ring_A
    
    ; Else button pressed -> get out from this state
    RCALL Disable_LED_B
    RCALL Delay_200ms
    LDI state_mach,w84btn_press
    RET
    
State_w84press_A_Ring_A:
    RCALL Ringing_LED_A
    RET

State_w84press_B:
    RCALL Scan_B
    
    CPI tmp,0
    BREQ State_w84press_B_Ring_B
    
    ; Else button pressed -> get out from this state
    RCALL Disable_LED_A
    RCALL Delay_200ms
    LDI state_mach,w84btn_press
    RET
    
State_w84press_B_Ring_B:
    RCALL Ringing_LED_B
    RET
